<?php

/**
 * @file
 * Provides functions for translation provider nativy.com.
 */

/**
 * Implements hook_menu().
 */
function tmgmt_nativy_menu() {
  $items['tmgmt/nativy/callback/%tmgmt_job'] = array(
    'title' => 'Nativy Notification',
    'description' => 'Callback for the completion notification of a translation job.',
    'type' => MENU_CALLBACK,
    'page callback' => 'tmgmt_nativy_process_callback',
    'page arguments' => array(3),
    'access callback' => TRUE,
  );
  return $items;
}

/**
 * Implements hook_tmgmt_translator_plugin_info().
 */
function tmgmt_nativy_tmgmt_translator_plugin_info() {
  return array(
    'nativy' => array(
      'label' => t('Nativy translator'),
      'description' => t('Nativy translator plugin that returns translations from nativy.com.'),
      'plugin controller class' => 'TMGMTNativyTranslatorPluginController',
      'ui controller class' => 'TMGMTNativyTranslatorUIController',
    ),
  );
}

/**
 * Callback page.
 *
 * @TODO Real processing. For now we just log whatever comes in.
 */
function tmgmt_nativy_process_callback(TMGMTJob $job) {
  $controller = $job->getTranslatorController();
  if (!($controller instanceof TMGMTNativyTranslatorPluginController)) {
    $job->addMessage('Job does not belong to nativy.', array(), 'error');
    return;
  }

  // Verify the validity of this request.
  if (!empty($_GET['timestamp']) && !empty($_GET['api_sign'])) {
    if (!$controller->verify($job->getTranslator(), $_GET['timestamp'], $_GET['api_sign'])) {
      $job->addMessage('Callback request has invalid api sign', array(), 'error');
      return;
    }
  }
  else {
    $job->addMessage('Callback request is missing api sign.', array(), 'error');
    return;
  }

  // Record payment success message.
  if (!empty($_GET['paid']) && $_GET['paid']) {
    $job->addMessage('Payment successfully recorded.');
    return;
  }

  if (!empty($_GET['text_to_key'])) {
    if ($success = $controller->nativyRetrieveSingleTranslation($job, tmgmt_ensure_keys_array($_GET['text_to_key']))) {
      $job->addMessage('Successfully retrieved single translation.');
    }
    else {
      $job->addMessage('Failed to retrieve single translation.', 'warning');
    }
  }
  else {
    if ($success = $controller->retrieveTranslation($job)) {
      $job->addMessage('Successfully retrieved translation.');
    }
    else {
      $job->addMessage('Failed to retrieve the translation.', 'warning');
    }
  }
}

function tmgmt_nativy_job_fetch_translation_submit($form, &$form_state) {
  $job = $form_state['tmgmt_job'];
  if ($job->getTranslatorController()->retrieveTranslation($job)) {
    drupal_set_message(t('Translation fetched fron Nativy.'));
  } else {
   drupal_set_message(t('Translation could not be retreived.'), 'error');
  }
}

/**
 * Save the chosen job settings.
 */
function tmgmt_nativy_job_settings_submit($form, &$form_state) {
  $job = $form_state['tmgmt_job'];
  $job = entity_ui_form_submit_build_entity($form, $form_state);
  $job->save();
}

/**
 * Post new chat message.
 */
function tmgmt_nativy_chat_message_submit($form, &$form_state) {
  if (trim($form_state['values']['new_message_text'])) {
    $job = $form_state['tmgmt_job'];
    $sent = $job->getTranslatorController()->sendChatMessage($job, $form_state['values']['new_message_text']);
    if ($sent) {
      drupal_set_message(t('Message is sent.'));
    }
    else {
      drupal_set_message(t('Message not sent.'), 'error');
    }
  }
  else {
    drupal_set_message(t('Please enter your message.'), 'warning');
  }
}

/**
 * A function that builds chat table.
 */
function tmgmt_nativy_chat_list($messages) {
  if (!empty($messages)) {
    $header = array();
    $rows = array();

    foreach ($messages as $message) {
      $name = $message->sender == 'client' ? t('You') : $message->translator_firstname;
      $image_url = $message->sender == 'client' ? $message->client_picture_link : $message->translator_picture_link;
      $image_html = theme('image', array('path' => $image_url, 'alt' => $name, 'title' => $name));
      $rows[] = array($name, $image_html, format_date($message->timestamp, 'short'), $message->message);
    }

    return theme('table', array('header' => $header, 'rows' => $rows));
  }

  return t('No chat messages.');
}
